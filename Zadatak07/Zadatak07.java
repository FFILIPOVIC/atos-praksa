package Zadatak07;

import java.util.Scanner;

public class Zadatak07 {

	public static void main(String[] args) {
		
		int i, counter = 0;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Unesite prvi prirodan broj:\n");
		int a = in.nextInt();
		while(a<=0) {
			System.out.println("Uneseni broj nije prirodan!\n");
			System.out.println("Unesite prvi prirodan broj:\n");
			a = in.nextInt();
		}
		System.out.println("Unesite drugi prirodan broj:\n");
		int b = in.nextInt();
		while(b<=0 || b<a) {
			System.out.println("Uneseni broj nije prirodan ili je manji od prvoga!\nPonovno unesite prirodan broj:\n");
			b = in.nextInt();
		}
		in.close();
		
		for(i=a;i<b+1;i++) {
			
			if(i%6==0) {
				counter++;
			}
		}
		System.out.printf("U intervalu od %d do %d, djeljivih brojeva sa 6 je %d.", a, b, counter);
		
	}

}
