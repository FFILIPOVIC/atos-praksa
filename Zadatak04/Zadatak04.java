package Zadatak04;

public class Zadatak04 {
	public static void main(String[] args) {
		int[] polje = {2, 11, 28, 55, 121};
		int i;
		
		for(i=0;i<polje.length;i++) {
			if(polje[i]%2==0){
				System.out.println(polje[i] + " je paran broj");
			}
			else {
				System.out.println(polje[i] + " je neparan broj");
			}
			if(polje[i]%3==0) {
				System.out.println(polje[i] + " je višekratnik od 3");
			}
			else if(polje[i]%5==0) {
				System.out.println(polje[i] + " je višekratnik od 5");
			}
			else if(polje[i]%11==0) {
				System.out.println(polje[i] + " je višekratnik od 11");
			}
		}
			
	}
}

