package Zadatak05;

public class Zadatak05 {

	public static void main(String[] args) {
		
		int start = -5;
		int end = 105;
		
		int counter = 0;
		int i;
		
		if(start>=10 || end<=100) {
			System.out.println("Pocetak i kraj intervala nisu ispravno definirani.");
		}
		
		else {
			
			for(i=start;i<end;i++) {
				
				if(i<=18) {
					counter+=4;
				}
				
				else if(i>18) {
					
					if(i%20==0) {
						continue;
					}
					
					if(i>=75) {
						break;
					}
					
					counter-=1;
				}
				//System.out.println("broj: " + i + ", brojac: " + counter);
			}
			System.out.println("brojac: " + counter);
		}
	}

}
